export type SongInfo = {
  titles: string[];
  artists: Artists;
  type: SongType;
};

export type Artists = string[] | undefined;

export enum SongType {
  Cover,
  Remix,
  Original,
  Unknown,
}
