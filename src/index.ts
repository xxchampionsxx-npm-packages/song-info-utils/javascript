import * as types from './types';

const specialCharacterRegex = /\/\\\[\]\(\)-\|×/i;

export function getSongInfo(fullTitle: string, artist?: string) {
  const title = unify(fullTitle);

  const titles: string[] = [];
  let artists: types.Artists = [];
  let type: types.SongType = types.SongType.Unknown;

  if (!hasSpecialCharacters(title)) {
    titles.push(fullTitle);
    artists = undefined;
    type = types.SongType.Unknown;
  } else {
    type = findSongType(title);
  }

  const info: types.SongInfo = {
    titles: titles,
    artists: artists,
    type: type,
  };

  return info;
}

export function unify(title: string): string {
  const replace = [
    { from: '『', to: '[' },
    { from: '』', to: ']' },
    { from: '／', to: '/' },
    { from: '【', to: '[' },
    { from: '【', to: '[' },
    { from: '】', to: ']' },
    { from: '「', to: '[' },
    { from: '」', to: ']' },
  ];
  replace.forEach((replace) => {
    title.replace(replace.from, replace.to);
  });
  return title;
}

export function hasSpecialCharacters(title: string): boolean {
  return specialCharacterRegex.test(title);
}

export function findSongType(title: string): types.SongType {
  title = title.toLowerCase();

  let type: types.SongType = types.SongType.Unknown;

  if (title.includes('cover') || title.includes('covered')) {
    type = types.SongType.Cover;
  } else if (title.includes('remix')) {
    type = types.SongType.Remix;
  } else if (title.includes('original')) {
    type = types.SongType.Original;
  }

  return type;
}

export const siu = {
  getSongInfo,
  unify,
};
